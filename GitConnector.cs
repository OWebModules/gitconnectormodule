﻿using GitConnectorModule.Models;
using GitConnectorModule.Services;
using LibGit2Sharp;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitConnectorModule
{
    public class GitConnector : AppController
    {
        public async Task<ResultOItem<ClassObject>> GetClassObjectReference(clsOntologyItem refItem)
        {
            var serviceElastic = new ElasticAgent(Globals);

            var reference = await serviceElastic.GetClassObject(refItem);

            if (reference.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return reference;
            }

            if (reference.Result.ObjectItem.GUID_Parent != Config.LocalData.Class_GitConnectorModule.GUID)
            {
                reference.ResultState = Globals.LState_Nothing.Clone();
                reference.ResultState.Additional1 = "The Reference is no TFS Connector Config";
            }

            return reference;
        }

        public async Task<ResultItem<GetGitCommitsResult>> GetGitCommits(GetGitCommitsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetGitCommitsResult>>(async () =>
            {
                var result = new ResultItem<GetGitCommitsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new GetGitCommitsResult()
                };

                if (!request.IsValid)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Request-Error: {request.ValidationMessage}";
                    return result;
                }

                var elasticAgent = new ElasticAgent(Globals);

                var serviceResult = await elasticAgent.GetGitCommits(request);

                result.ResultState = serviceResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                result = serviceResult;
                if (request.RequestType.HasFlag(RequestType.Project))
                {
                    result.Result.GitCommitsToProjects = result.Result.GitCommitsToProjects.Where(rel => rel.ID_Other == request.IdProject).ToList();
                    result.Result.CommitDateTimes = (from dateTime in result.Result.CommitDateTimes
                                                     join commitToProject in result.Result.GitCommitsToProjects on dateTime.ID_Object equals commitToProject.ID_Object
                                                     select dateTime).ToList();

                    result.Result.CommitMessages = (from message in result.Result.CommitMessages
                                                    join commitToProject in result.Result.GitCommitsToProjects on message.ID_Object equals commitToProject.ID_Object
                                                    select message).ToList();

                    
                    
                }

                if (request.RequestType.HasFlag(RequestType.DateRange))
                {
                    if (request.CommitRangeStart != null && request.CommitRangeEnd != null)
                    {
                        result.Result.CommitDateTimes = result.Result.CommitDateTimes.Where(dateTime => dateTime.Val_Date >= request.CommitRangeStart.Value && dateTime.Val_Date <= request.CommitRangeEnd.Value).ToList();
                        result.Result.GitCommitsToProjects = (from dateTime in result.Result.CommitDateTimes
                                                              join commitToProject in result.Result.GitCommitsToProjects on dateTime.ID_Object equals commitToProject.ID_Object
                                                              select commitToProject).ToList();

                        result.Result.CommitMessages = (from message in result.Result.CommitMessages
                                                        join commitToProject in result.Result.GitCommitsToProjects on message.ID_Object equals commitToProject.ID_Object
                                                        select message).ToList();
                    }
                    else if (request.CommitRangeStart != null)
                    {
                        result.Result.CommitDateTimes = result.Result.CommitDateTimes.Where(dateTime => dateTime.Val_Date >= request.CommitRangeStart.Value).ToList();
                        result.Result.GitCommitsToProjects = (from dateTime in result.Result.CommitDateTimes
                                                              join commitToProject in result.Result.GitCommitsToProjects on dateTime.ID_Object equals commitToProject.ID_Object
                                                              select commitToProject).ToList();

                        result.Result.CommitMessages = (from message in result.Result.CommitMessages
                                                        join commitToProject in result.Result.GitCommitsToProjects on message.ID_Object equals commitToProject.ID_Object
                                                        select message).ToList();
                    }
                    else if (request.CommitRangeEnd != null)
                    {
                        result.Result.CommitDateTimes = result.Result.CommitDateTimes.Where(dateTime => dateTime.Val_Date <= request.CommitRangeEnd.Value).ToList();
                        result.Result.GitCommitsToProjects = (from dateTime in result.Result.CommitDateTimes
                                                              join commitToProject in result.Result.GitCommitsToProjects on dateTime.ID_Object equals commitToProject.ID_Object
                                                              select commitToProject).ToList();

                        result.Result.CommitMessages = (from message in result.Result.CommitMessages
                                                        join commitToProject in result.Result.GitCommitsToProjects on message.ID_Object equals commitToProject.ID_Object
                                                        select message).ToList();
                    }


                }

                if (request.RequestType.HasFlag(RequestType.TextFilter))
                {
                    result.Result.CommitMessages = result.Result.CommitMessages.Where(message => message.Val_String.ToLower().Contains(request.CommitTextFilter.ToLower())).ToList();

                    result.Result.GitCommitsToProjects = (from message in result.Result.CommitMessages
                                                          join commitToProject in result.Result.GitCommitsToProjects on message.ID_Object equals commitToProject.ID_Object
                                                          select commitToProject).ToList();

                    result.Result.CommitDateTimes = (from dateTime in result.Result.CommitDateTimes
                                                     join commitToProject in result.Result.GitCommitsToProjects on dateTime.ID_Object equals commitToProject.ID_Object
                                                     select dateTime).ToList();

                    
                }

                if (request.RequestType.HasFlag(RequestType.TextFilterRegex))
                {
                    result.Result.CommitMessages = result.Result.CommitMessages.Where(message => request.CommitTextFilterRegex.Match(message.Val_String).Success).ToList();

                    result.Result.GitCommitsToProjects = (from message in result.Result.CommitMessages
                                                          join commitToProject in result.Result.GitCommitsToProjects on message.ID_Object equals commitToProject.ID_Object
                                                          select commitToProject).ToList();

                    result.Result.CommitDateTimes = (from dateTime in result.Result.CommitDateTimes
                                                     join commitToProject in result.Result.GitCommitsToProjects on dateTime.ID_Object equals commitToProject.ID_Object
                                                     select dateTime).ToList();
                }

                return result;
            });
            return taskResult;
        }

        public async Task<ResultItem<SyncGitProjectsResult>> SyncGitProjects(SyncGitProjectsRequest request)
        {

            var result = new ResultItem<SyncGitProjectsResult>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new SyncGitProjectsResult()
            };

            var elasticService = new ElasticAgent(Globals);

            request.MessageOutput?.OutputInfo($"Get Model...");
            var serviceResult = await elasticService.GetModel(request);

            result.ResultState = serviceResult.ResultState;

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                return result;
            }
            request.MessageOutput?.OutputInfo($"Have Model");

            var taskResult = await Task.Run<ResultItem<SyncGitProjectsResult>>(async () =>
            {
                var asyncResult = new ResultItem<SyncGitProjectsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new SyncGitProjectsResult()
                };

                
                var configs = (from config in serviceResult.Result.Configs
                               join proj in serviceResult.Result.ConfigsToProjects on config.GUID equals proj.ID_Object
                               join path in serviceResult.Result.ConfigsToFolders on config.GUID equals path.ID_Object
                               select new { config, path, proj });

                request.MessageOutput?.OutputInfo($"Config-Count: {configs.Count()}");

                var relationConfig = new clsRelationConfig(Globals);
                var gitConfigItem = new GitConfig();

                foreach (var config in configs)
                {
                    request.MessageOutput?.OutputInfo($"Config: {config.config.Name}");
                    var gitConfigRepo = new ConfigRepositoryRelation();
                    gitConfigItem.ConfigRepoRel.Add(gitConfigRepo);
                    gitConfigRepo.ProjectItem = new clsOntologyItem
                    {
                        GUID = config.proj.ID_Other,
                        Name = config.proj.Name_Other,
                        GUID_Parent = config.proj.ID_Parent_Other,
                        Type = Globals.Type_Object
                    };

                    request.MessageOutput?.OutputInfo($"Project: {gitConfigRepo.ProjectItem.Name}");
                    request.MessageOutput?.OutputInfo($"Get repositories...");
                    try
                    {
                        using (var repo = new Repository(config.path.Name_Other))
                        {
                            gitConfigRepo.Project = repo;
                            gitConfigRepo.Name = config.proj.Name_Other;
                            gitConfigRepo.StandardBranch = repo.Head.FriendlyName;

                            result.ResultState = GetSubProjects(gitConfigItem, repo, config.path);

                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                                return result;
                            }

                            request.MessageOutput?.OutputInfo($"Save commits...");
                            result.ResultState = await SaveCommits(gitConfigRepo.Project, gitConfigRepo.ProjectItem, elasticService, relationConfig);

                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                                return result;
                            }
                            request.MessageOutput?.OutputInfo($"Saved commits.");
                        }
                    }
                    catch (Exception ex)
                    {

                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Error while getting Repositories: {ex.Message}";
                        return result;
                    }

                    var projectsToCheck = new List<string> { gitConfigRepo.Name };

                    var subModuleRels = gitConfigItem.SubProjectRelations.Where(repo => repo.ParentProject == gitConfigRepo.Project);
                    projectsToCheck.AddRange(subModuleRels.Select(proj => proj.SubModule.Name));

                    var checkResultProjects = await elasticService.CheckProjects(projectsToCheck);

                    result.ResultState = checkResultProjects.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    request.MessageOutput?.OutputInfo($"Projects to check: {checkResultProjects.Result.Count}");

                    (from subProject in checkResultProjects.Result
                     join subModule in subModuleRels on subProject.Name equals subModule.NameSubProject
                     select new { subProject, subModule }).ToList().ForEach(proj =>
                      {
                          proj.subModule.SubModuleItem = proj.subProject;
                      });

                    request.MessageOutput?.OutputInfo($"Submodule-Rels: {subModuleRels.Count()}");
                    foreach (var subModule in subModuleRels)
                    {
                        using (var repo = new Repository(subModule.SubModulePath))
                        {
                            result.ResultState = await SaveCommits(repo, subModule.SubModuleItem, elasticService, relationConfig);

                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                                return result;
                            }
                        }
                            
                    }

                    var urlsToCheck = subModuleRels.Select(proj => proj.SubModule.Url).ToList();

                    request.MessageOutput?.OutputInfo($"Check Urls: {urlsToCheck.Count()}");
                    var checkResultUrls = await elasticService.CheckUrls(urlsToCheck);

                    result.ResultState = checkResultUrls.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    request.MessageOutput?.OutputInfo($"Checked Urls");

                    var branchesToCheck = subModuleRels.Select(proj => proj.StandardBranch).ToList();

                    request.MessageOutput?.OutputInfo($"Check branches...");
                    var checkResultBranches = await elasticService.CheckBranches(branchesToCheck);
                    result.ResultState = checkResultBranches.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    request.MessageOutput?.OutputInfo($"Checked branches");

                    (from proj in subModuleRels
                     join dbProj in checkResultProjects.Result on proj.NameSubProject equals dbProj.Name
                     join dbUrl in checkResultUrls.Result on proj.SubModule.Url equals dbUrl.Name
                     join dbBranch in checkResultBranches.Result on proj.StandardBranch equals dbBranch.Name
                     select new { proj, dbProj, dbUrl, dbBranch }).ToList().ForEach(proj =>
                     {
                         proj.proj.SubModuleItem = proj.dbProj;
                         proj.proj.UrlItem = proj.dbUrl;
                         proj.proj.BranchItem = proj.dbBranch;
                     });

                    

                    var projSubmoduleRels = (from subModule in subModuleRels
                                             select relationConfig.Rel_ObjectRelation(new clsOntologyItem { GUID = config.proj.ID_Other, Name = config.proj.Name_Other, GUID_Parent = Config.LocalData.Class_Projects__Gitlab_.GUID, Type = Globals.Type_Object }, subModule.SubModuleItem, Config.LocalData.RelationType_submodules)).ToList();

                    var projSubmoduleToSave = (from projSubmoduleRel in projSubmoduleRels
                                               join projToSubProj in serviceResult.Result.ProjectsToProjects.Where(projToProj => projToProj.ID_Object == config.proj.ID_Other) on new { projSubmoduleRel.ID_Object, projSubmoduleRel.ID_Other, projSubmoduleRel.ID_RelationType }
                                                    equals new { projToSubProj.ID_Object, projToSubProj.ID_Other, projToSubProj.ID_RelationType } into projToSubProjs
                                               from projToSubProj in projToSubProjs.DefaultIfEmpty()
                                               where projToSubProj == null
                                               select projSubmoduleRel).ToList();

                    var projSubmoduleToDelete = (from projToSubProj in serviceResult.Result.ProjectsToProjects.Where(projToProj => projToProj.ID_Object == config.proj.ID_Other)
                                                 join projSubmoduleRel in projSubmoduleRels on new { projToSubProj.ID_Object, projToSubProj.ID_Other, projToSubProj.ID_RelationType } equals
                                                    new { projSubmoduleRel.ID_Object, projSubmoduleRel.ID_Other, projSubmoduleRel.ID_RelationType } into projSubmoduleRels1
                                                 from projSubmoduleRel in projSubmoduleRels1.DefaultIfEmpty()
                                                 where projSubmoduleRel == null
                                                 select projToSubProj).ToList();

                    if (projSubmoduleToSave.Any())
                    {
                        request.MessageOutput?.OutputInfo($"Save Project to Project Relations: {projSubmoduleToSave.Count} ...");
                        var saveResult = await elasticService.SaveProjectToProjectRelation(projSubmoduleToSave);

                        result.ResultState = saveResult;

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }
                        request.MessageOutput?.OutputInfo($"Saved Project to Project Relations");
                    }


                    if (projSubmoduleToDelete.Any())
                    {
                        request.MessageOutput?.OutputInfo($"Delete Project to Project Relations: {projSubmoduleToDelete.Count} ...");
                        var delResult = await elasticService.DeleteProjectToProjectRelation(projSubmoduleToDelete);

                        result.ResultState = delResult;

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }
                        request.MessageOutput?.OutputInfo($"Deleted Project to Project Relations");
                    }


                    var projToUrlRels = (from subModule in subModuleRels
                                         select relationConfig.Rel_ObjectRelation(subModule.SubModuleItem, subModule.UrlItem, Config.LocalData.Object_OItem_belonging_Source)).ToList();

                    var projToUrls = (from projToSubProj in serviceResult.Result.ProjectsToProjects.Where(proj => proj.ID_Object == config.proj.ID_Other)
                                      join projToUrl in serviceResult.Result.ProjectsToUrls on projToSubProj.ID_Other equals projToUrl.ID_Object
                                      select new { projToSubProj, projToUrl });
                    var urlsToSave = (from projToUrlRel in projToUrlRels
                                      join projToUrl in projToUrls on new { projToUrlRel.ID_Object, projToUrlRel.ID_Other } equals new { projToUrl.projToUrl.ID_Object, projToUrl.projToUrl.ID_Other } into projToUrls1
                                      from projToUrl in projToUrls1.DefaultIfEmpty()
                                      where projToUrl == null
                                      select projToUrlRel).ToList();

                    var urlsToDel = (from projToUrl in projToUrls
                                     join projToUrlRel in projToUrlRels on new { projToUrl.projToUrl.ID_Object, projToUrl.projToUrl.ID_Other } equals new { projToUrlRel.ID_Object, projToUrlRel.ID_Other } into projToUrlRels1
                                     from projToUrlRel in projToUrlRels1.DefaultIfEmpty()
                                     where projToUrlRel == null
                                     select projToUrl.projToUrl).ToList();
                    if (urlsToSave.Any())
                    {
                        request.MessageOutput?.OutputInfo($"Save Urls: {urlsToSave.Count}");
                        var saveResult = await elasticService.SaveProjectToUrlsRelation(urlsToSave);

                        result.ResultState = saveResult;

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }
                        request.MessageOutput?.OutputInfo($"Saved Urls");
                    }


                    if (urlsToDel.Any())
                    {
                        request.MessageOutput?.OutputInfo($"Delete Url-Relations: {urlsToDel.Count}");
                        var delResult = await elasticService.DeleteProjectToUrlRelation(urlsToDel);

                        result.ResultState = delResult;

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }
                        request.MessageOutput?.OutputInfo($"Deleted Url-Relations");
                    }

                    var standardBranches = (from standardBranchToProjectDb in serviceResult.Result.StandardBranchToProject
                                           join standardBranchToBranchDb in serviceResult.Result.StandardBranchToProject on standardBranchToProjectDb.ID_Object equals standardBranchToBranchDb.ID_Object
                                           join standardBranchToConfigDb in serviceResult.Result.StandardBranchToConfig.Where(conf => conf.ID_Other == config.config.GUID) on standardBranchToProjectDb.ID_Object equals standardBranchToConfigDb.ID_Object
                                           select new { standardBranchToProjectDb, standardBranchToBranchDb }).ToList();

                    var standardBranchesPre = (from standardBranchToProject in subModuleRels
                                                  join standardBranchDb in standardBranches on
                                                    new { IdSubmodule = standardBranchToProject.SubModuleItem.GUID, IdBranch = standardBranchToProject.BranchItem.GUID } equals
                                                    new { IdSubmodule = standardBranchDb.standardBranchToProjectDb.ID_Other, IdBranch = standardBranchDb.standardBranchToBranchDb.ID_Other } into standardBranchDbs
                                                  from standardBranchDb in standardBranchDbs.DefaultIfEmpty()
                                                  where standardBranchDb == null
                                                  select new
                                                  {
                                                      standardBranchToProject,
                                                      standardBranch = new clsOntologyItem
                                                      {
                                                          GUID = Globals.NewGUID,
                                                          Name = standardBranchToProject.SubModuleItem.Name + standardBranchToProject.BranchItem.Name,
                                                          GUID_Parent = Config.LocalData.Class_Standard_Branch__Git_.GUID,
                                                          Type = Globals.Type_Object
                                                      }
                                                  }).ToList();

                    standardBranchesPre.ForEach(stdBranch =>
                    {
                        if (stdBranch.standardBranch.Name.Length > 255)
                        {
                            stdBranch.standardBranch.Name = stdBranch.standardBranch.Name.Substring(0, 254);
                        }
                    });


                    var standardBranchesToSave = standardBranchesPre.Select(branch => branch.standardBranch).ToList();

                    if (standardBranchesToSave.Any())
                    {
                        request.MessageOutput?.OutputInfo($"Standard branches to save: {standardBranchesToSave.Count}");
                        var saveResult = await elasticService.SaveStandardBranches(standardBranchesToSave);

                        result.ResultState = saveResult;

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }
                        request.MessageOutput?.OutputInfo($"Saved Standard branches");
                    }

                    var relStandardBranchesToProjectsToSave = standardBranchesPre.Select(br => relationConfig.Rel_ObjectRelation(br.standardBranch, br.standardBranchToProject.SubModuleItem, Config.LocalData.RelationType_belongs_to)).ToList();

                    if (relStandardBranchesToProjectsToSave.Any())
                    {
                        request.MessageOutput?.OutputInfo($"Save standard branches to submodules: {relStandardBranchesToProjectsToSave.Count}");
                        result.ResultState = await elasticService.SaveStandardBranchesToSubmodules(relStandardBranchesToProjectsToSave);

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving standard branches to projects";
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }
                        request.MessageOutput?.OutputInfo($"Saved standard branches to submodules");
                    }
                    

                    var relStandardBranchesToBranchesToSave = standardBranchesPre.Select(br => relationConfig.Rel_ObjectRelation(br.standardBranch, br.standardBranchToProject.BranchItem, Config.LocalData.RelationType_belongs_to)).ToList();

                    if (relStandardBranchesToBranchesToSave.Any())
                    {
                        request.MessageOutput?.OutputInfo($"Save Standard branches to brnaches: {relStandardBranchesToBranchesToSave.Count}");
                        result.ResultState = await elasticService.SaveStandardBranchesToBranches(relStandardBranchesToBranchesToSave);

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving standard branches to branches";
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }
                        request.MessageOutput?.OutputInfo($"Saved Standard branches to branches");
                    }

                    var relStandardBranchesToConfigToSave = standardBranchesPre.Select(br => relationConfig.Rel_ObjectRelation(br.standardBranch, config.config, Config.LocalData.RelationType_belongs_to)).ToList();

                    if (relStandardBranchesToConfigToSave.Any())
                    {
                        request.MessageOutput?.OutputInfo($"Save Standard branches to configs: {relStandardBranchesToConfigToSave.Count}");
                        result.ResultState = await elasticService.SaveStandardBranchesToConfigs(relStandardBranchesToConfigToSave);

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving standard branches to Config";
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }

                        request.MessageOutput?.OutputInfo($"Saved Standard branches to configs");
                    }
                }




                return asyncResult;
            });

            if (taskResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return taskResult;
            }





            return taskResult;

        }

        private async Task<clsOntologyItem> SaveCommits(Repository repo, clsOntologyItem projectItem, ElasticAgent elasticService, clsRelationConfig relationConfig)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async() =>
            {
                var result = Globals.LState_Success.Clone();

                var commits = repo.Commits.Select(commit => new clsOntologyItem
                {
                    Name = commit.Id.Sha,
                    GUID_Parent = Config.LocalData.Class_Git_Commit.GUID,
                    Type = Globals.Type_Object,

                }).ToList();

                result = await elasticService.CheckCommits(commits);

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }
                var newCommits = commits.Where(commit => commit.New_Item.Value);
                var lastIdResult = await elasticService.GetLastOrderIdProjectToCommits(new clsOntologyItem
                {
                    GUID_Parent = Config.LocalData.Class_Git_Commit.GUID
                }, new clsOntologyItem
                {
                    GUID = projectItem.GUID
                },
                Config.LocalData.RelationType_belongs_to);

                result = lastIdResult.ResultState;

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while getting the last orderId between project and commits";
                    return result;
                }
                var orderId = lastIdResult.Result + 1;

                var commitToProject = newCommits.Select(commit => relationConfig.Rel_ObjectRelation(commit, projectItem, Config.LocalData.RelationType_belongs_to, false, orderId++)).ToList();

                result = await elasticService.SaveCommitsToProject(commitToProject);

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var attribsToSave = new List<clsObjectAtt>();
                (from commit in newCommits
                                join gitCommit in repo.Commits on commit.Name equals gitCommit.Id.Sha
                                select new { commit, gitCommit }).ToList().ForEach(commit => 
                                {
                                    var dateTimeStamp = relationConfig.Rel_ObjectAttribute(commit.commit, Config.LocalData.AttributeType_DateTimestamp, commit.gitCommit.Committer.When.DateTime);
                                    var message = relationConfig.Rel_ObjectAttribute(commit.commit, Config.LocalData.AttributeType_Message, commit.gitCommit.Message);

                                    attribsToSave.Add(dateTimeStamp);
                                    attribsToSave.Add(message);
                                });

                result = await elasticService.SaveAttributes(attribsToSave);

                return result;
            });

            return taskResult;
            
        }

        public clsOntologyItem GetSubProjects(GitConfig gitConfig, Repository parentRepo, clsObjectRel configPathRel)
        {
            var result = Globals.LState_Success.Clone();

            var subModules = parentRepo.Submodules;

            foreach (var submodule in subModules)
            {
                try
                {
                    var path = Path.Combine(configPathRel.Name_Other, submodule.Path);
                    using (var subRepo = new Repository(path))
                    {

                        var subProjectRel = new SubProjectItem
                        {
                            ParentProject = parentRepo,
                            SubProject = subRepo,
                            NameSubProject = submodule.Name,
                            SubModule = submodule,
                            StandardBranch = subRepo.Head.FriendlyName,
                            SubModulePath = path
                        };

                        gitConfig.SubProjectRelations.Add(subProjectRel);
                    }
                }
                catch (Exception ex)
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = $"Error while getting Submodules:{ex.Message}";
                    return result;

                }

            }

            return result;
        }

        public GitConnector(Globals globals) : base(globals)
        {
        }
    }
}
