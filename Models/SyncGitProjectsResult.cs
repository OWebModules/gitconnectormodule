﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitConnectorModule.Models
{
    public class SyncGitProjectsResult
    {
        public Models.GitConnectorModule Config { get; set; }
    }
}
