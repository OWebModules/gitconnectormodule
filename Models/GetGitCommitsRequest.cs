﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GitConnectorModule.Models
{
    [Flags]
    public enum RequestType
    {
        AllCommits = 0,
        TextFilter = 1,
        TextFilterRegex = 2,
        DateRange = 4,
        Project = 8
    }
    public class GetGitCommitsRequest
    {
        public RequestType RequestType { get; private set; }

        public string IdConfig { get; private set; }
        
        public bool IsValid { get; private set; }
        public string ValidationMessage { get; private set; }

        private string idProject;
        public string IdProject
        {
            get { return idProject; }
            set
            {
                idProject = value;
                RequestType |= RequestType.Project;
                ValidateRequest();
            }
        }

        private string commitTextFilter;
        public string CommitTextFilter
        {
            get { return commitTextFilter; }
            set
            {
                commitTextFilter = value;
                RequestType |= RequestType.TextFilter;
                ValidateRequest();
            }
        }

        private Regex commitTextFilterRegex;
        public Regex CommitTextFilterRegex
        {
            get { return commitTextFilterRegex; }
            set
            {
                commitTextFilterRegex = value;
                RequestType |= RequestType.TextFilterRegex;
                ValidateRequest();
            }
        }

        private DateTime? commitRangeStart;
        public DateTime? CommitRangeStart
        {
            get { return commitRangeStart; }
            set
            {
                commitRangeStart = value;
                RequestType |= RequestType.DateRange;
                ValidateRequest();
            }
        }

        private DateTime? commitRangeEnd;
        public DateTime? CommitRangeEnd
        {
            get { return commitRangeEnd; }
            set
            {
                commitRangeEnd = value;
                RequestType |= RequestType.DateRange;
            }
        }

        public IMessageOutput MessageOutput { get; set; }

        private void ValidateRequest()
        {
            IsValid = true;
            ValidationMessage = "";
            if (RequestType.HasFlag(RequestType.TextFilter) &&
                RequestType.HasFlag(RequestType.TextFilterRegex))
            {
                IsValid = false;
                ValidationMessage = "Only Textfilter or Regex Textfilter is valid not both!";
                return;
            }

            if (RequestType.HasFlag(RequestType.DateRange) &&
                CommitRangeStart == null && CommitRangeEnd == null)
            {
                IsValid = false;
                ValidationMessage = "Daterange must be set: start -> end; -> end; start ->";
                return;
            }


        }
        
        public GetGitCommitsRequest(string idConfig)
        {
            IdConfig = idConfig;
            
        }

        
    }
}
