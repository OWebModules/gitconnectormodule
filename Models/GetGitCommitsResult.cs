﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitConnectorModule.Models
{
    public class GetGitCommitsResult
    {
        public List<clsObjectRel> GitCommitsToProjects { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> CommitMessages { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectAtt> CommitDateTimes { get; set; } = new List<clsObjectAtt>();
    }
}
