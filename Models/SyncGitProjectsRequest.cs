﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitConnectorModule.Models
{
    public class SyncGitProjectsRequest
    {
        public string IdConfig { get; private set; }

        public IMessageOutput MessageOutput { get; set; }

        public SyncGitProjectsRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
