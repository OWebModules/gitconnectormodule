﻿using LibGit2Sharp;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitConnectorModule.Models
{
    public class GitConfig
    {
        public List<ConfigRepositoryRelation> ConfigRepoRel { get; set; } = new List<ConfigRepositoryRelation>();
        
        public List<SubProjectItem> SubProjectRelations { get; set; } = new List<SubProjectItem>();

    }

    public class ConfigRepositoryRelation
    {
        public Models.GitConfig Config { get; set; }
        public string Name { get; set; }
        public Repository Project { get; set; }
        public clsOntologyItem ProjectItem { get; set; }
        public string StandardBranch { get; set; }
        public clsOntologyItem BranchItem { get; set; }
    }

    public class SubProjectItem
    {
        public Repository ParentProject { get; set; }
        public string NameSubProject { get; set; }
        public Repository SubProject { get; set; }
        public Submodule SubModule { get; set; }
        public clsOntologyItem SubModuleItem { get; set; }
        public clsOntologyItem UrlItem { get; set; }
        public string StandardBranch { get; set; }
        public clsOntologyItem BranchItem { get; set; }
        public string SubModulePath { get; set; }
    }
}
