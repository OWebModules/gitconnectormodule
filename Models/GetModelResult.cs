﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitConnectorModule.Models
{
    public class GetModelResult
    {
        public clsOntologyItem RootConfig { get; set; }

        public List<clsOntologyItem> Configs { get; set; } = new List<clsOntologyItem>();

        public List<clsObjectRel> ConfigsToProjects { get; set; } = new List<clsObjectRel>();

        public List<clsOntologyItem> Projects { get; set; } = new List<clsOntologyItem>();

        public List<clsObjectRel> ProjectsToProjects { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ProjectsToUrls { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ConfigsToFolders { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> StandardBranchToProject { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> StandardBranchToBranch { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> StandardBranchToConfig { get; set; } = new List<clsObjectRel>();
    }
}
