﻿using GitConnectorModule.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitConnectorModule.Services
{
    public class ElasticAgent : ElasticBaseAgent
    {

        public async Task<ResultItem<GetGitCommitsResult>> GetGitCommits(GetGitCommitsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetGitCommitsResult>>(() =>
           {
               var result = new ResultItem<GetGitCommitsResult>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new GetGitCommitsResult()
               };

               var searchCommits = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Parent_Object = Config.LocalData.Class_Git_Commit.GUID,
                       ID_Parent_Other = Config.LocalData.Class_Projects__Gitlab_.GUID
                   }
               };

               var dbReaderCommitsToProjects = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderCommitsToProjects.GetDataObjectRel(searchCommits);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the relations between commits and projects!";
                   return result;
               }

               result.Result.GitCommitsToProjects = dbReaderCommitsToProjects.ObjectRels;

               var searchCommitMessages = result.Result.GitCommitsToProjects.Select(commit => new clsObjectAtt
               {
                   ID_Object = commit.ID_Object,
                   ID_AttributeType = Config.LocalData.AttributeType_Message.GUID
               }).ToList();

               if (searchCommitMessages.Any())
               {
                   var dbReaderCommitMessages = new OntologyModDBConnector(globals);
                   result.ResultState = dbReaderCommitMessages.GetDataObjectAtt(searchCommitMessages);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the commit-messages!";
                       return result;
                   }

                   result.Result.CommitMessages = dbReaderCommitMessages.ObjAtts;
               }

               var searchCommitDateTimes = result.Result.GitCommitsToProjects.Select(commit => new clsObjectAtt
               {
                   ID_Object = commit.ID_Object,
                   ID_AttributeType = Config.LocalData.AttributeType_DateTimestamp.GUID
               }).ToList();

               if (searchCommitDateTimes.Any())
               {
                   var dbReaderCommitDateTimes = new OntologyModDBConnector(globals);
                   result.ResultState = dbReaderCommitDateTimes.GetDataObjectAtt(searchCommitDateTimes);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the commit-datetimes!";
                       return result;
                   }

                   result.Result.CommitDateTimes = dbReaderCommitDateTimes.ObjAtts;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveProjects(List<clsOntologyItem> projects)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbWriter = new OntologyModDBConnector(globals);

                result = dbWriter.SaveObjects(projects);

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> CheckCommits(List<clsOntologyItem> commits)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbReaderCommits = new OntologyModDBConnector(globals);

                if (!commits.Any())
                {
                    return result;
                }

                result = dbReaderCommits.GetDataObjects(commits);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while getting the commits";
                    return result;
                }

                (from commit in commits
                 join dbCommit in dbReaderCommits.Objects1 on commit.Name equals dbCommit.Name into dbCommits
                 from dbCommit in dbCommits.DefaultIfEmpty()
                 select new { commit, dbCommit }).ToList().ForEach(commit =>
                  {
                      if (commit.dbCommit == null)
                      {
                          commit.commit.GUID = globals.NewGUID;
                          commit.commit.New_Item = true;
                      }
                      else
                      {
                          commit.commit.GUID = commit.dbCommit.GUID;
                          commit.commit.New_Item = false;
                      }
                  });

                var commitsToSave = commits.Where(commit => commit.New_Item.Value).ToList();

                

                if (commitsToSave.Any())
                {
                    var dbWriter = new OntologyModDBConnector(globals);
                    result = dbWriter.SaveObjects(commitsToSave);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the commits!";
                    }
                }
                
                return result;
            });
            return taskResult;
        }

        public async Task<clsOntologyItem> SaveStandardBranches(List<clsOntologyItem> standardBranches)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbWriter = new OntologyModDBConnector(globals);

                result = dbWriter.SaveObjects(standardBranches);

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveAttributes(List<clsObjectAtt> attributes)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                if (!attributes.Any()) return result;

                var dbWriter = new OntologyModDBConnector(globals);

                result = dbWriter.SaveObjAtt(attributes);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<long>> GetLastOrderIdProjectToCommits(clsOntologyItem projectItem, clsOntologyItem commitItem, clsOntologyItem relationType)
        {
            var taskResult = await Task.Run<ResultItem<long>>(() =>
            {
                var result = new ResultItem<long>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var dbReader = new OntologyModDBConnector(globals);

                result.Result = dbReader.GetDataRelOrderId(projectItem, commitItem, relationType, false);

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveCommitsToProject(List<clsObjectRel> relations)
        {
            return await SaveRelations(relations);
        }

        public async Task<clsOntologyItem> SaveProjectToProjectRelation(List<clsObjectRel> relations)
        {
            return await SaveRelations(relations);
        }

        private async Task<clsOntologyItem> SaveRelations(List<clsObjectRel> relations)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                if (!relations.Any()) return result;

                var dbWriter = new OntologyModDBConnector(globals);

                result = dbWriter.SaveObjRel(relations);

                return result;
            });

            return taskResult;

            
        }

        public async Task<clsOntologyItem> SaveProjectToUrlsRelation(List<clsObjectRel> relations)
        {
            return await SaveRelations(relations);
        }

        public async Task<clsOntologyItem> SaveStandardBranchesToSubmodules(List<clsObjectRel> relations)
        {
            return await SaveRelations(relations);
        }

        public async Task<clsOntologyItem> SaveStandardBranchesToBranches(List<clsObjectRel> relations)
        {
            return await SaveRelations(relations);
        }

        public async Task<clsOntologyItem> SaveStandardBranchesToConfigs(List<clsObjectRel> relations)
        {
            return await SaveRelations(relations);
        }


        public async Task<clsOntologyItem> DeleteProjectToProjectRelation(List<clsObjectRel> relations)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbDeletor = new OntologyModDBConnector(globals);

                result = dbDeletor.DelObjectRels(relations);

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> DeleteProjectToUrlRelation(List<clsObjectRel> relations)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbDeletor = new OntologyModDBConnector(globals);

                result = dbDeletor.DelObjectRels(relations);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> CheckProjects(List<string> projectList)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                projectList = projectList.GroupBy(proj => proj).Select(proj => proj.Key).ToList();
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                var searchProjs = projectList.Select(url => new clsOntologyItem
                {
                    Name = url,
                    GUID_Parent = Config.LocalData.Class_Projects__Gitlab_.GUID
                }).ToList();

                var dbConnector = new OntologyModDBConnector(globals);

                result.ResultState = dbConnector.GetDataObjects(searchProjs);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting Projects";
                    return result;
                }

                result.Result = (from url in projectList
                                 join exist in dbConnector.Objects1 on url equals exist.Name into exists
                                 from exist in exists.DefaultIfEmpty()
                                 select new { url, exist }).Select(urlExist =>
                                 {
                                     var resultNewUrl = urlExist.exist;
                                     if (resultNewUrl == null)
                                     {
                                         resultNewUrl = new clsOntologyItem
                                         {
                                             GUID = globals.NewGUID,
                                             Name = urlExist.url,
                                             GUID_Parent = Config.LocalData.Class_Projects__Gitlab_.GUID,
                                             Type = globals.Type_Object,
                                             New_Item = true
                                         };
                                     }

                                     return resultNewUrl;
                                 }).ToList();


                var projectsToSave = result.Result.Where(prj => prj.New_Item != null && prj.New_Item.Value).ToList();

                if (projectsToSave.Any())
                {
                    result.ResultState = dbConnector.SaveObjects(projectsToSave);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving projects";
                        return result;
                    }

                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> CheckUrls(List<string> urls)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                urls = urls.GroupBy(proj => proj).Select(proj => proj.Key).ToList();
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                var searchUrls = urls.Select(url => new clsOntologyItem
                {
                    Name = url,
                    GUID_Parent = Config.LocalData.Class_Url.GUID
                }).ToList();

                var dbConnector = new OntologyModDBConnector(globals);

                result.ResultState = dbConnector.GetDataObjects(searchUrls);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting Urls";
                    return result;
                }

                result.Result = (from url in urls
                                  join exist in dbConnector.Objects1 on url equals exist.Name into exists
                                  from exist in exists.DefaultIfEmpty()
                                  select new { url, exist }).Select(urlExist =>
                                  {
                                      var resultNewUrl = urlExist.exist;
                                      if (resultNewUrl == null)
                                      {
                                          resultNewUrl = new clsOntologyItem
                                          {
                                              GUID = globals.NewGUID,
                                              Name = urlExist.url,
                                              GUID_Parent = Config.LocalData.Class_Url.GUID,
                                              Type = globals.Type_Object,
                                              New_Item = true
                                          };
                                      }

                                      return resultNewUrl;
                                  }).ToList();


                var urlsToSave = result.Result.Where(url => url.New_Item != null && url.New_Item.Value).ToList();

                if (urlsToSave.Any())
                {
                    result.ResultState = dbConnector.SaveObjects(urlsToSave);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving urls";
                        return result;
                    }

                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> CheckBranches(List<string> branches)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                branches = branches.GroupBy(proj => proj).Select(proj => proj.Key).ToList();
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                var searchBranches = branches.Select(branch => new clsOntologyItem
                {
                    Name = branch,
                    GUID_Parent = Config.LocalData.Class_Branch__Git_.GUID
                }).ToList();

                var dbConnector = new OntologyModDBConnector(globals);

                result.ResultState = dbConnector.GetDataObjects(searchBranches);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting branches";
                    return result;
                }

                result.Result = (from branch in branches
                                 join exist in dbConnector.Objects1 on branch equals exist.Name into exists
                                 from exist in exists.DefaultIfEmpty()
                                 select new { branch, exist }).Select(urlExist =>
                                 {
                                     var resultNewUrl = urlExist.exist;
                                     if (resultNewUrl == null)
                                     {
                                         resultNewUrl = new clsOntologyItem
                                         {
                                             GUID = globals.NewGUID,
                                             Name = urlExist.branch,
                                             GUID_Parent = Config.LocalData.Class_Branch__Git_.GUID,
                                             Type = globals.Type_Object,
                                             New_Item = true
                                         };
                                     }

                                     return resultNewUrl;
                                 }).ToList();


                var branchToSave = result.Result.Where(branch => branch.New_Item != null && branch.New_Item.Value).ToList();

                if (branchToSave.Any())
                {
                    result.ResultState = dbConnector.SaveObjects(branchToSave);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving branches";
                        return result;
                    }

                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<GetModelResult>> GetModel(SyncGitProjectsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetModelResult>>(() =>
            {
                var result = new ResultItem<GetModelResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new GetModelResult()
                };

                var searchRootConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig,
                        GUID_Parent = Config.LocalData.Class_GitConnectorModule.GUID
                    }
                };

                var dbReaderRootConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderRootConfig.GetDataObjects(searchRootConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting root-config";
                    return result;
                }

                result.Result.RootConfig = dbReaderRootConfig.Objects1.FirstOrDefault();

                if (result.Result.RootConfig == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Root-Config found!";
                    return result;
                }

                var searchSubConfigs = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.RootConfig.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_GitConnectorModule_contains_GitConnectorModule.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_GitConnectorModule_contains_GitConnectorModule.ID_Class_Right
                    }
                };

                var dbReaderSubConfigs = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSubConfigs.GetDataObjectRel(searchSubConfigs);


                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting sub-configs";
                    return result;
                }

                if (dbReaderSubConfigs.ObjectRels.Any())
                {
                    result.Result.Configs = dbReaderSubConfigs.ObjectRels.Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology
                    }).ToList();
                }
                else
                {
                    result.Result.Configs.Add(result.Result.RootConfig);
                }

                var searchPaths = result.Result.Configs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_GitConnectorModule_located_at_Path.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_GitConnectorModule_located_at_Path.ID_Class_Right
                }).ToList();

                if (searchPaths.Any())
                {
                    var dbReaderPaths = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderPaths.GetDataObjectRel(searchPaths);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting Paths";
                        return result;
                    }

                    result.Result.ConfigsToFolders = dbReaderPaths.ObjectRels;
                }

                var searchRelProjects = result.Result.Configs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_GitConnectorModule_belonging_Projects__Gitlab_.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_GitConnectorModule_belonging_Projects__Gitlab_.ID_Class_Right
                }).ToList();

                if (searchRelProjects.Any())
                {
                    var dbReaderRelProjects = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderRelProjects.GetDataObjectRel(searchRelProjects);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting Projects";
                        return result;
                    }

                    result.Result.ConfigsToProjects = dbReaderRelProjects.ObjectRels;
                }

                var searchProjects = new List<clsOntologyItem>{
                    new clsOntologyItem
                    {
                        GUID_Parent = Config.LocalData.Class_Projects__Gitlab_.GUID
                    }
                };

                var dbReaderProjects = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderProjects.GetDataObjects(searchProjects);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting Projects";
                    return result;
                }

                result.Result.Projects = dbReaderProjects.Objects1;

                var searchSubProjects = new List<clsObjectRel>
                {
                   new clsObjectRel
                   {
                       ID_Parent_Object = Config.LocalData.ClassRel_Projects__Gitlab__submodules_Projects__Gitlab_.ID_Class_Left,
                       ID_RelationType = Config.LocalData.ClassRel_Projects__Gitlab__submodules_Projects__Gitlab_.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_Projects__Gitlab__submodules_Projects__Gitlab_.ID_Class_Right
                   }
                };

                var dbReaderProjectsToProjects = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderProjectsToProjects.GetDataObjectRel(searchSubProjects);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting Projects to Projects relation";
                    return result;
                }

                result.Result.ProjectsToProjects = dbReaderProjectsToProjects.ObjectRels;

                var searchUrls = result.Result.Projects.Select(proj => new clsObjectRel
                {
                    ID_Object = proj.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Projects__Gitlab__belonging_Source_Url.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Projects__Gitlab__belonging_Source_Url.ID_Class_Right
                }).ToList();

                if (searchUrls.Any())
                {
                    var dbReaderProjectsToUrl = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderProjectsToUrl.GetDataObjectRel(searchUrls);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting Project-Urls";
                        return result;
                    }

                    result.Result.ProjectsToUrls = dbReaderProjectsToUrl.ObjectRels;
                }

                var searchStandardBranchesToProjects = result.Result.Projects.Select(proj => new clsObjectRel
                {
                    ID_Other = proj.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Standard_Branch__Git__belongs_to_Projects__Gitlab_.ID_RelationType,
                    ID_Parent_Object = Config.LocalData.ClassRel_Standard_Branch__Git__belongs_to_Projects__Gitlab_.ID_Class_Left
                }).ToList();
                
                if (searchStandardBranchesToProjects.Any())
                {
                    var dbReaderStandardBranchToProjects = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderStandardBranchToProjects.GetDataObjectRel(searchStandardBranchesToProjects);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting relation between Standard-branches and projects";
                        return result;
                    }

                    result.Result.StandardBranchToProject = dbReaderStandardBranchToProjects.ObjectRels;
                }

                var searchStandardBranchesToBranches = result.Result.StandardBranchToProject.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_RelationType = Config.LocalData.ClassRel_Standard_Branch__Git__belongs_to_Branch__Git_.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Standard_Branch__Git__belongs_to_Branch__Git_.ID_Class_Right
                }).ToList();

                if (searchStandardBranchesToBranches.Any())
                {
                    var dbReaderStandardBranchesToBranches = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderStandardBranchesToBranches.GetDataObjectRel(searchStandardBranchesToBranches);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the relation between Standard-branches and branches";
                        return result;
                    }

                    result.Result.StandardBranchToBranch = dbReaderStandardBranchesToBranches.ObjectRels;
                }

                var searchStandardBranchesToConfigs = result.Result.StandardBranchToProject.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_RelationType = Config.LocalData.ClassRel_Standard_Branch__Git__belongs_to_GitConnectorModule.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Standard_Branch__Git__belongs_to_GitConnectorModule.ID_Class_Right
                }).ToList();

                if (searchStandardBranchesToConfigs.Any())
                {
                    var dbReaderStandardBranchesToConfigs = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderStandardBranchesToConfigs.GetDataObjectRel(searchStandardBranchesToConfigs);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the relation between Standard-branches and config";
                        return result;
                    }

                    result.Result.StandardBranchToBranch = dbReaderStandardBranchesToConfigs.ObjectRels;
                }

                return result;
            });

            return taskResult;

        }

        public ElasticAgent(Globals globals) : base(globals)
        {
            this.globals = globals;
        }
    }
}
